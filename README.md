## Kanban

This project serves as a personal kanban for myself using Gitlab's awesome issue board feature.

There is nothing here.

## For myself  
### Study Board: [Study](https://gitlab.com/LER0ever/kanban/boards/484536?=&label_name[]=0-Study)  
### Study List: [Study List](https://gitlab.com/LER0ever/kanban/issues?label_name%5B%5D=0-Study&scope=all&sort=due_date&state=opened)
### Development Board: [Dev](https://gitlab.com/LER0ever/kanban/boards?=&label_name[]=0-Development)
### Development List: [Development List](https://gitlab.com/LER0ever/kanban/issues?label_name%5B%5D=0-Development&scope=all&sort=due_date&state=opened)
### General Board: [General](https://gitlab.com/LER0ever/kanban/boards/483929?=&label_name[]=0-General)  
